#include <stdio.h>
#include <stdint.h>
#include <string>
#include <cmath>
#include <algorithm>

#define FILTER_WIDTH 3

using namespace std;
// code của thầy
#define CHECK(call)\
{\
    const cudaError_t error = call;\
    if (error != cudaSuccess)\
    {\
        fprintf(stderr, "Error: %s:%d, ", __FILE__, __LINE__);\
        fprintf(stderr, "code: %d, reason: %s\n", error,\
                cudaGetErrorString(error));\
        exit(EXIT_FAILURE);\
    }\
}

struct GpuTimer
{
    cudaEvent_t start;
    cudaEvent_t stop;

    GpuTimer()
    {
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
    }

    ~GpuTimer()
    {
        cudaEventDestroy(start);
        cudaEventDestroy(stop);
    }

    void Start()
    {
        cudaEventRecord(start, 0);                                                                 
        cudaEventSynchronize(start);
    }

    void Stop()
    {
        cudaEventRecord(stop, 0);
    }

    float Elapsed()
    {
        float elapsed;
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&elapsed, start, stop);
        return elapsed;
    }
};
// code của thầy
void readPnm(char * fileName, int &width, int &height, uchar3 * &pixels)
{
    FILE * f = fopen(fileName, "r");
    if (f == NULL)
    {
        printf("Cannot read %s\n", fileName);
        exit(EXIT_FAILURE);
    }

    char type[3];
    fscanf(f, "%s", type);
    
    if (strcmp(type, "P3") != 0) // In this exercise, we don't touch other types
    {
        fclose(f);
        printf("Cannot read %s\n", fileName); 
        exit(EXIT_FAILURE); 
    }

    fscanf(f, "%i", &width);
    fscanf(f, "%i", &height);
    
    int max_val;
    fscanf(f, "%i", &max_val);
    if (max_val > 255) // In this exercise, we assume 1 byte per value
    {
        fclose(f);
        printf("Cannot read %s\n", fileName); 
        exit(EXIT_FAILURE); 
    }

    pixels = (uchar3 *)malloc(width * height * sizeof(uchar3));
    for (int i = 0; i < width * height; i++)
        fscanf(f, "%hhu%hhu%hhu", &pixels[i].x, &pixels[i].y, &pixels[i].z);

    fclose(f);
}
// code của thầy
void writePnm(uchar3 *pixels, int width, int height, int originalWidth, char *fileName)
{
    FILE * f = fopen(fileName, "w");
    if (f == NULL)
    {
        printf("Cannot write %s\n", fileName);
        exit(EXIT_FAILURE);
    }   

    fprintf(f, "P3\n%i\n%i\n255\n", width, height); 

    for (int r = 0; r < height; ++r) {
        for (int c = 0; c < width; ++c) {
            int i = r * originalWidth + c;
            fprintf(f, "%hhu\n%hhu\n%hhu\n", pixels[i].x, pixels[i].y, pixels[i].z);
        }
    }
    
    fclose(f);
}
// code thầy
void printDeviceInfo()
{
    cudaDeviceProp devProv;
    CHECK(cudaGetDeviceProperties(&devProv, 0));
    printf("**********GPU info**********\n");
    printf("Name: %s\n", devProv.name);
    printf("Compute capability: %d.%d\n", devProv.major, devProv.minor);
    printf("Num SMs: %d\n", devProv.multiProcessorCount);
    printf("Max num threads per SM: %d\n", devProv.maxThreadsPerMultiProcessor); 
    printf("Max num warps per SM: %d\n", devProv.maxThreadsPerMultiProcessor / devProv.warpSize);
    printf("GMEM: %lu bytes\n", devProv.totalGlobalMem);
    printf("CMEM: %lu bytes\n", devProv.totalConstMem);
    printf("L2 cache: %i bytes\n", devProv.l2CacheSize);
    printf("SMEM / one SM: %lu bytes\n", devProv.sharedMemPerMultiprocessor);

    printf("****************************\n\n");

}
int xSobel[3][3] = {{1,0,-1},{2,0,-2},{1,0,-1}};
int ySobel[3][3] = {{1,2,1},{0,0,0},{-1,-2,-1}};
/*
   cài đặt tuần tự - Nguyễn Văn Tuấn - co tham khao tu git
*/
void convolution(uint8_t *inPixels, int width, int height, int *outPixels, const int *filter) 
{
    // duyet moi row output pixel
    for (int outRow = 0; outRow < height; outRow++)
		{
            // duyet cot
			for (int outCol = 0; outCol < width; outCol++)
			{
				int outPixel = 0;
				for (int filterRow = 0; filterRow < FILTER_WIDTH; filterRow++)
				{
					for (int filterC = 0; filterC < FILTER_WIDTH; filterC++)
					{
						int value = filter[filterRow*FILTER_WIDTH + filterC];
						int inPixelsR = outRow - FILTER_WIDTH/2 + filterRow;
						int inPixelsC = outCol - FILTER_WIDTH/2 + filterC;
						inPixelsR = min(max(0, inPixelsR), height - 1);
						inPixelsC = min(max(0, inPixelsC), width - 1);
						uint8_t inPixel = inPixels[inPixelsR*width + inPixelsC];
						outPixel += value * inPixel;
					}
				}
				outPixels[outRow*width + outCol] = outPixel; 
			}
		}
}
void convertRgb2GrayImage(uint8_t *inPixels, uint8_t *grayPixels) {
    for (int i = 0; i < height; i++) 
    {
        for (int j = 0; j < width; j++)
        {
            int idx = width*i + j;
            grayPixels[idx] = 0.299f*inPixels[3*idx] + 0.587f*inPixels[3*idx + 1] + 0.114f*inPixels[3*idx + 2];
        }
    }
}
// tham khảo từ link git 
// Tìm độ quan trọng của mỗi pixel (edge detection)
void edgeDetection(uint8_t *inPixels, int width, int height, uint *importancePixels) 
{
    // Chuyển ảnh RGB sang ảnh grayscale: gray = 0.299*red + 0.587*green + 0.114*blue 
    uint8_t *grayPixels = (uint8_t*)malloc(width*height*sizeof(uint8_t));
    convertRgb2GrayImage(inPixels,grayPixels) {
    

    // Phát hiện cạnh theo chiều x: Convolution với bộ lọc x-sobel
    int *edgePixels_x = (int*)malloc(width*height*sizeof(int));
    convolution(grayPixels, width, height, edgePixels_x, x_sobel_filter);

    // Phát hiện cạnh theo chiều y: Convolution với bộ lọc y-sobel
    int *edgePixels_y = (int*)malloc(width*height*sizeof(int));
    convolution(grayPixels, width, height, edgePixels_y, y_sobel_filter);

    // Tính độ quan trọng của một pixel
    for (int i = 0; i < width*height; i++)
        importancePixels[i] = abs(edgePixels_x[i]) + abs(edgePixels_y[i]);

    // Giải phóng vùng nhớ
    free(grayPixels);
    free(edgePixels_x);
    free(edgePixels_y);
}
void findLeastImportantSeam(uint *importancePixels, int width, int height, int *seam)
{
    uint *importanceOfSeams = importancePixels;
    int *trace = (int*)malloc(width*(height - 1)*sizeof(int)); // bỏ trace của dòng cuối

    // Tính độ quan trọng ít nhất tính tới dưới cùng
    for (int r = height - 1; r >= 0; r--)
    {
        for (int c = 0; c < width; c++)
        {
            // Đoạn này có cách nào code đẹp hơn ko-------
            int left = INT_MAX;
            int right = INT_MAX;
            int down = importanceOfSeams[width*(r + 1) + c]; 
            if (c - 1 >= 0 && c + 1 < width)
            {
                left = importanceOfSeams[width*(r + 1) + c - 1];
                right = importanceOfSeams[width*(r + 1) + c + 1];
            }
            else if (c - 1 < 0)
                right = importanceOfSeams[width*(r + 1) + c + 1];
            else
                left = importanceOfSeams[width*(r + 1) + c - 1];

            int minNextElem = min(min(left, down), right);
            importanceOfSeams[width*r + c] += minNextElem;
            if (minNextElem == down)
                trace[width*r + c] = 0;
            else if (minNextElem == left)
                trace[width*r + c] = -1;
            else
                trace[width*r + c] = 1;
            //----------------------------------------------
        }
    }

    // Truy vết và tìm seam ít quan trọng nhất
    seam[0] = 0;
    for (int i = 1; i < width; i++)
        if (importanceOfSeams[seam[0]] > importanceOfSeams[i])
            seam[0] = i;
    for (int i = 1; i < height; i++)
        seam[i] = seam[i - 1] + width + trace[seam[i - 1]];

    // Free vùng nhớ
    free(trace);
}
void removeSeam(uint8_t *pixels, int width, int height, int *seam)
{
    for (int i = 0; i < height - 1; i++)
    {
        for (int j = 3*(seam[i] - i); j < 3*seam[i + 1]; j++)
        {
            pixels[j] = pixels[j + 3*(i + 1)];
        }
    }

    for (int j = 3*(seam[height - 1] - height + 1); j < 3*(width*height - height); j++)
        pixels[j] = pixels[j + 3*height];
}

// Seam carving by host
void seamCarvingByHost(uint8_t *inPixels, int width, int height, uint8_t *outPixels, int newWidth)
{
    GpuTimer timer;
    timer.Start();
    uint *importancePixels = (uint*)malloc(width*height*sizeof(uint));
    int *seam = (int*)malloc(height*sizeof(int));

    memcpy(outPixels, inPixels, 3*width*height*sizeof(uint8_t));

    while (width > newWidth)
    {
        importancePixels = (uint*)realloc(importancePixels, width*height*sizeof(uint));

        // Tìm độ quan trọng của mỗi pixel (edge detection)
        edgeDetection(outPixels, width, height, importancePixels);
        // Tìm seam ít quan trọng nhất
        findLeastImportantSeam(importancePixels, width, height, seam);
        // Xóa seam này
        removeSeam(outPixels, width, height, seam);
        width -= 1;
    }

    // Giải phóng vùng nhớ
    free(importancePixels);
    free(seam);
    timer.Stop();
    float time = timer.Elapsed();
    printf("Processing time (use host): %f ms\n\n", time);
}

// cai dat song song
__constant__ int d_xSobel[9];
__constant__ int d_ySobel[9];

__device__ int d_originalWidth;

__global__ void convertRgb2GrayKernel(uchar3 * inPixels, int width, int height, uint8_t * outPixels) {
    size_t row = blockIdx.y * blockDim.y + threadIdx.y;
    size_t col = blockIdx.x * blockDim.x + threadIdx.x;
    size_t idx = row * width + col;
    if (row < height && col < width) {
        outPixels[idx] = 0.299f * inPixels[idx].x
                    + 0.587f * inPixels[idx].y
                    + 0.114f * inPixels[idx].z;
    }
}

__global__ void computePriorityKernel(uint8_t * inPixels, int width, int height, int * priority) {
    int filterWidth = 3;

    size_t row = blockIdx.y * blockDim.y + threadIdx.y;
    size_t col = blockIdx.x * blockDim.x + threadIdx.x;

    size_t s_width = blockDim.x + filterWidth - 1;
    size_t s_height = blockDim.y + filterWidth - 1;

    // Each block loads data from GMEM to SMEM
    extern __shared__ uint8_t s_inPixels[];

    int readRow = row - (filterWidth >> 1), readCol, tmpRow, tmpCol;
    int firstReadCol = col - (filterWidth >> 1);
    size_t virtualRow = threadIdx.y, virtualCol;
    for (; virtualRow < s_height; readRow += blockDim.y, virtualRow += blockDim.y) {
        tmpRow = readRow;
        if (readRow < 0) {
            readRow = 0;
        } else if (readRow >= height) {
            readRow = height - 1;
        }
        readCol = firstReadCol;
        virtualCol = threadIdx.x;
        for (; virtualCol < s_width; readCol += blockDim.x, virtualCol += blockDim.x) {
            tmpCol = readCol;
            if (readCol < 0) {
                readCol = 0;
            } else if (readCol >= width) {
                readCol = width - 1;
            }
            s_inPixels[virtualRow * s_width + virtualCol] = inPixels[readRow * d_originalWidth + readCol];
            readCol = tmpCol;
        }
        readRow = tmpRow;
    } 
    __syncthreads();
    // ---------------------------------------

    // Each thread compute priority on SMEM
    int x = 0, y = 0;
    for (int i = 0; i < filterWidth; ++i) {
        for (int j = 0; j < filterWidth; ++j) {
            uint8_t closest = s_inPixels[(threadIdx.y + i) * s_width + threadIdx.x + j];
            size_t filterIdx = i * filterWidth + j;
            x += closest * d_xSobel[filterIdx];
            y += closest * d_ySobel[filterIdx];
        }
    }

    // Each thread writes result from SMEM to GMEM
    if (col < width && row < height) {
        size_t idx = row * d_originalWidth + col;
        priority[idx] = abs(x) + abs(y);
    }
}

__global__ void computeScoreKernel(int * priority, int * score, int width, int height, int row) {
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    if (col < width) {
        if (row == 0) {
            score[col] = priority[col];
        } else {
            int idx = row * d_originalWidth + col;
            int aboveIdx = (row - 1) * d_originalWidth + col;

            int min = score[aboveIdx];
            if (col > 0 && score[aboveIdx - 1] < min) {
                min = score[aboveIdx - 1];
            }
            if (col < width - 1 && score[aboveIdx + 1] < min) {
                min = score[aboveIdx + 1];
            }

            score[idx] = min + priority[idx];
        }
    }
}

__global__ void carvingKernel(int *leastSignificantPixel, uchar3 *outPixels, uint8_t *grayPixels, int * priority, int width) {
    int row = blockIdx.x;
    int from = leastSignificantPixel[row];
    int baseIdx = row * d_originalWidth;
    for (int i = from; i < width - 1; ++i) {
        outPixels[baseIdx + i] = outPixels[baseIdx + i + 1];
        grayPixels[baseIdx + i] = grayPixels[baseIdx + i + 1];
        priority[baseIdx + i] = priority[baseIdx + i + 1];
    }
}

void seamCarvingByDevice(uchar3 *inPixels, int width, int height, int targetWidth, uchar3* outPixels, dim3 blockSize) {
    GpuTimer timer;
    timer.Start();

    CHECK(cudaMemcpyToSymbol(d_originalWidth, &width, sizeof(int)));

    // copy input to device
    uchar3 *d_inPixels;
    CHECK(cudaMalloc(&d_inPixels, width * height * sizeof(uchar3)));
    CHECK(cudaMemcpy(d_inPixels, inPixels, width * height * sizeof(uchar3), cudaMemcpyHostToDevice));

    // turn input image to grayscale
    uint8_t * d_grayPixels;
    CHECK(cudaMalloc(&d_grayPixels, width * height * sizeof(uint8_t)));    
    
    dim3 gridSize((width-1)/blockSize.x + 1, (height-1)/blockSize.y + 1);
    convertRgb2GrayKernel<<<gridSize, blockSize>>>(d_inPixels, width, height, d_grayPixels);
    cudaDeviceSynchronize();
    CHECK(cudaGetLastError());

    // compute pixel priority
    int * d_priority;
    CHECK(cudaMalloc(&d_priority, width * height * sizeof(int)));

    size_t smemSize = ((blockSize.x + 3 - 1) * (blockSize.y + 3 - 1)) * sizeof(uint8_t);
    computePriorityKernel<<<gridSize, blockSize, smemSize>>>(d_grayPixels, width, height, d_priority);
    cudaDeviceSynchronize();
    CHECK(cudaGetLastError());

    // allocate neccessary memory
    int * score = (int *)malloc(width * height * sizeof(int));
    int * leastSignificantPixel = (int *)malloc(height * sizeof(int));
    int * priority = (int *)malloc(width * height * sizeof(int));
    int originalWidth = width;

    int * d_leastSignificantPixel;
    CHECK(cudaMalloc(&d_leastSignificantPixel, height * sizeof(int)));

    // carve until reach desired width
    while (width > targetWidth) {
        // compute min seam table
        CHECK(cudaMemcpy(priority, d_priority, originalWidth * height * sizeof(int), cudaMemcpyDeviceToHost));
        for (int c = 0; c < width; ++c) {
            score[c] = priority[c];
        }
        for (int r = 1; r < height; ++r) {
            for (int c = 0; c < width; ++c) {
                int idx = r * originalWidth + c;
                int aboveIdx = (r - 1) * originalWidth + c;

                int min = score[aboveIdx];
                if (c > 0 && score[aboveIdx - 1] < min) {
                    min = score[aboveIdx - 1];
                }
                if (c < width - 1 && score[aboveIdx + 1] < min) {
                    min = score[aboveIdx + 1];
                }

                score[idx] = min + priority[idx];
            }
        }

        // find least significant pixel index of each row and store in d_leastSignificantPixel (SEQUENTIAL, in kernel or host)
        int minCol = 0, r = height - 1;
        for (int c = 1; c < width; ++c) {
            if (score[r * originalWidth + c] < score[r * originalWidth + minCol])
                minCol = c;
        }
        for (; r >= 0; --r) {
            leastSignificantPixel[r] = minCol;
            if (r > 0) {
                int aboveIdx = (r - 1) * originalWidth + minCol;
                int min = score[aboveIdx], minColCpy = minCol;
                if (minColCpy > 0 && score[aboveIdx - 1] < min) {
                    min = score[aboveIdx - 1];
                    minCol = minColCpy - 1;
                }
                if (minColCpy < width - 1 && score[aboveIdx + 1] < min) {
                    minCol = minColCpy + 1;
                }
            }
        }
        CHECK(cudaMemcpy(d_leastSignificantPixel, leastSignificantPixel, height * sizeof(int), cudaMemcpyHostToDevice));

        // carve
        carvingKernel<<<height, 1>>>(d_leastSignificantPixel, d_inPixels, d_grayPixels, d_priority, width);
        cudaDeviceSynchronize();
        CHECK(cudaGetLastError());
        
        --width;

        // update priority
        computePriorityKernel<<<gridSize, blockSize, smemSize>>>(d_grayPixels, width, height, d_priority);
        cudaDeviceSynchronize();
        CHECK(cudaGetLastError());
    }

    CHECK(cudaMemcpy(outPixels, d_inPixels, originalWidth * height * sizeof(uchar3), cudaMemcpyDeviceToHost));

    CHECK(cudaFree(d_inPixels));
    CHECK(cudaFree(d_grayPixels));
    CHECK(cudaFree(d_priority));
    CHECK(cudaFree(d_leastSignificantPixel));

    free(score);
    free(leastSignificantPixel);
    free(priority);

    timer.Stop();
    float time = timer.Elapsed();
    printf("Processing time (use device): %f ms\n\n", time);    
}

uint8_t getClosest(uint8_t *pixels, int r, int c, int width, int height, int originalWidth)
{
    if (r < 0) {
        r = 0;
    } else if (r >= height) {
        r = height - 1;
    }

    if (c < 0) {
        c = 0;
    } else if (c >= width) {
        c = width - 1;
    }

    return pixels[r * originalWidth + c];
}



float computeError(uchar3 * a1, uchar3 * a2, int n)
{
    float err = 0;
    for (int i = 0; i < n; i++)
    {
        err += abs((int)a1[i].x - (int)a2[i].x);
        err += abs((int)a1[i].y - (int)a2[i].y);
        err += abs((int)a1[i].z - (int)a2[i].z);
    }
    err /= (n * 3);
    return err;
}

char *concatStr(const char * s1, const char * s2)
{
    char * result = (char *)malloc(strlen(s1) + strlen(s2) + 1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}



int main(int argc, char ** argv)
{   
    if (argc != 4 && argc != 6)
    {
        printf("The number of arguments is invalid\n");
        return EXIT_FAILURE;
    }

    printDeviceInfo();

    // Read input RGB image file
    int width, height;
    uchar3 *inPixels;
    readPnm(argv[1], width, height, inPixels);
    printf("Image size (width x height): %i x %i\n\n", width, height);

    float ratio = stof(argv[3]);
    if (ratio <= 0 || ratio > 1)
        return EXIT_FAILURE; // invalid ratio
    printf("Image carving ratio: %f\n\n", ratio);

    int targetWidth = int(width * ratio);

    // seam carving using host
    uchar3 * correctOutPixels = (uchar3 *)malloc(width * height * sizeof(uchar3));
    seamCarvingByHost(inPixels, width, height, targetWidth, correctOutPixels);

    // seam carving using device
    CHECK(cudaMemcpyToSymbol(d_xSobel, xSobel, 9 * sizeof(int)));
    CHECK(cudaMemcpyToSymbol(d_ySobel, ySobel, 9 * sizeof(int)));
    uchar3 * outPixels= (uchar3 *)malloc(width * height * sizeof(uchar3));
    dim3 blockSize(32, 32); // Default
    if (argc == 6)
    {
        blockSize.x = atoi(argv[4]);
        blockSize.y = atoi(argv[5]);
    } 
    seamCarvingByDevice(inPixels, width, height, targetWidth, outPixels, blockSize);

    // Compute mean absolute error between host result and device result
    float err = computeError(outPixels, correctOutPixels, width * height);
    printf("Error between device result and host result: %f\n", err);

    // Write results to files
    char *outFileNameBase = strtok(argv[2], "."); // Get rid of extension
    writePnm(correctOutPixels, targetWidth, height, width, concatStr(outFileNameBase, "_host.pnm"));
    writePnm(outPixels, targetWidth, height, width, concatStr(outFileNameBase, "_device.pnm"));

    // Free memories
    free(inPixels);
    free(correctOutPixels);
    free(outPixels);
}
