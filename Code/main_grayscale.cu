#include <stdio.h>
#include <stdint.h>
#include <string>
#include <cmath>
#include <algorithm>

using namespace std;

#define CHECK(call)\
{\
    const cudaError_t error = call;\
    if (error != cudaSuccess)\
    {\
        fprintf(stderr, "Error: %s:%d, ", __FILE__, __LINE__);\
        fprintf(stderr, "code: %d, reason: %s\n", error,\
                cudaGetErrorString(error));\
        exit(EXIT_FAILURE);\
    }\
}


int xSobel[3][3] = {{1,0,-1},{2,0,-2},{1,0,-1}};
int ySobel[3][3] = {{1,2,1},{0,0,0},{-1,-2,-1}};
int originalWidth;

__constant__ int d_originalWidth;

struct GpuTimer
{
    cudaEvent_t start;
    cudaEvent_t stop;

    GpuTimer()
    {
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
    }

    ~GpuTimer()
    {
        cudaEventDestroy(start);
        cudaEventDestroy(stop);
    }

    void Start()
    {
        cudaEventRecord(start, 0);                                                                 
        cudaEventSynchronize(start);
    }

    void Stop()
    {
        cudaEventRecord(stop, 0);
    }

    float Elapsed()
    {
        float elapsed;
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&elapsed, start, stop);
        return elapsed;
    }
};

void readPnm(char * fileName, int &width, int &height, uchar3 * &pixels)
{
    FILE * f = fopen(fileName, "r");
    if (f == NULL)
    {
        printf("Cannot read %s\n", fileName);
        exit(EXIT_FAILURE);
    }

    char type[3];
    fscanf(f, "%s", type);
    
    if (strcmp(type, "P3") != 0) // In this exercise, we don't touch other types
    {
        fclose(f);
        printf("Cannot read %s\n", fileName); 
        exit(EXIT_FAILURE); 
    }

    fscanf(f, "%i", &width);
    fscanf(f, "%i", &height);
    
    int max_val;
    fscanf(f, "%i", &max_val);
    if (max_val > 255) // In this exercise, we assume 1 byte per value
    {
        fclose(f);
        printf("Cannot read %s\n", fileName); 
        exit(EXIT_FAILURE); 
    }

    pixels = (uchar3 *)malloc(width * height * sizeof(uchar3));
    for (int i = 0; i < width * height; i++)
        fscanf(f, "%hhu%hhu%hhu", &pixels[i].x, &pixels[i].y, &pixels[i].z);

    fclose(f);
}

void writePnm(uchar3 *pixels, int width, int height, int originalWidth, char *fileName)
{
    FILE * f = fopen(fileName, "w");
    if (f == NULL)
    {
        printf("Cannot write %s\n", fileName);
        exit(EXIT_FAILURE);
    }   

    fprintf(f, "P3\n%i\n%i\n255\n", width, height); 

    for (int r = 0; r < height; ++r) {
        for (int c = 0; c < width; ++c) {
            int i = r * originalWidth + c;
            fprintf(f, "%hhu\n%hhu\n%hhu\n", pixels[i].x, pixels[i].y, pixels[i].z);
        }
    }
    
    fclose(f);
}

int computeEnergy(uint8_t *pixels, int row, int col, int width, int height ){
    int x = 0, y = 0, filterWidth = 3;
    for (int i = -1; i < filterWidth - 1; ++i) {
        for (int j = - 1; j < filterWidth - 1; ++j) {
            int _row = min(height-1, max(0, row + i));
            int _col = min(width-1, max(0, col + j));
            uint8_t closest = pixels[_row * originalWidth + _col];
            x += closest * xSobel[i+1][j+1];
            y += closest * ySobel[i+1][j+1];
        }
    }
   return abs(x) + abs(y);
}

void convertRgb2Gray(uchar3 *inPixels, int width, int height, uint8_t *outPixels){
        // Tạo ảnh grayscale
        for (int r = 0; r < height; ++r) {
            for (int c = 0; c < width; ++c) {
                int i = r * width + c;
                outPixels[i] = 0.299f * inPixels[i].x
                                + 0.587f * inPixels[i].y
                                + 0.114f * inPixels[i].z;
            }
        }
}

__global__ void convertRgb2GrayKernel(uchar3 * inPixels, int width, int height, uint8_t * outPixels) {
    size_t row = blockIdx.y * blockDim.y + threadIdx.y;
    size_t col = blockIdx.x * blockDim.x + threadIdx.x;
    size_t idx = row * width + col;
    if (row < height && col < width) {
        outPixels[idx] = 0.299f * inPixels[idx].x
                    + 0.587f * inPixels[idx].y
                    + 0.114f * inPixels[idx].z;
    }
}

void calculateEnergyTrack(int *energy, int *energyTrack, int width, int height){
    for (int c = 0; c < width; ++c) {
        energyTrack[c] = energy[c];
    }

    //Tính energyTrack
    for (int r = 1; r < height; ++r) {
        for (int c = 0; c < width; ++c) {
            int idx = r * originalWidth + c;
            int aboveIdx = (r - 1) * originalWidth + c;

            int min = energyTrack[aboveIdx];
            if (c > 0 && energyTrack[aboveIdx - 1] < min) {
                min = energyTrack[aboveIdx - 1];
            }
            if (c < width - 1 && energyTrack[aboveIdx + 1] < min) {
                min = energyTrack[aboveIdx + 1];
            }

            energyTrack[idx] = min + energy[idx];
        }
    }
}

void seamCarvingByDevice(uchar3 *inPixels, int width, int height, int finalWidth, uchar3* outPixels, dim3 blockSize) {
    GpuTimer timer;
    timer.Start();

    memcpy(outPixels, inPixels, width * height * sizeof(uchar3));

    // copy input to device
    uchar3 *d_inPixels;
    CHECK(cudaMalloc(&d_inPixels, width * height * sizeof(uchar3)));
    CHECK(cudaMemcpy(d_inPixels, inPixels, width * height * sizeof(uchar3), cudaMemcpyHostToDevice));

        
    // Tạo ảnh grayscale
    uint8_t * d_grayPixels;
    CHECK(cudaMalloc(&d_grayPixels, width * height * sizeof(uint8_t)));    
    
    dim3 gridSize((width-1)/blockSize.x + 1, (height-1)/blockSize.y + 1);
    convertRgb2GrayKernel<<<gridSize, blockSize>>>(d_inPixels, width, height, d_grayPixels);

    uint8_t *grayPixels= (uint8_t *)malloc(width * height * sizeof(uint8_t));
    CHECK(cudaMemcpy(grayPixels, d_grayPixels, width * height * sizeof(uint8_t), cudaMemcpyDeviceToHost));

    cudaDeviceSynchronize();
    CHECK(cudaGetLastError());
    // Bảng energy và bảng energyTrack
    int* energy = (int *)malloc(width * height * sizeof(int));
    int* energyTrack = (int *)malloc(width * height * sizeof(int));
    const int originalWidth = width;

    // Thực hiện xóa seam đến khi nào đạt được width mong muốn
    while (width > finalWidth) {

        // Tính energy Table
        for (int r = 0; r < height; ++r) {
            for (int c = 0; c < width; ++c) {
                energy[r * originalWidth + c] = computeEnergy(grayPixels,r,c,width,height);
            }
        }

        // Với hàng đầu tiên: energyTrack = energy
        for (int c = 0; c < width; ++c) {
            energyTrack[c] = energy[c];
        }

        /*
        Các hàng sau đó, với mỗi cột:
        - Lấy index của hàng phía trên
        - Tìm giá trị min(energyTrack của hàng phía trên, hàng phía trên trái nếu có, hàng phía trên phải nếu có)
        - Lấy min + giá trị energy tại điểm đó ra giá trị eneryTrack mới 
        */

        for (int r = 1; r < height; ++r) {
            for (int c = 0; c < width; ++c) {
                int idx = r * originalWidth + c;
                int aboveIdx = (r - 1) * originalWidth + c;

                int min = energyTrack[aboveIdx];
                if (c > 0 && energyTrack[aboveIdx - 1] < min) {
                    min = energyTrack[aboveIdx - 1];
                }
                if (c < width - 1 && energyTrack[aboveIdx + 1] < min) {
                    min = energyTrack[aboveIdx + 1];
                }

                energyTrack[idx] = min + energy[idx];
            }
        }
        // Đến đây ta có bảng energyTrack, thực hiện tìm cột có eneryTrack bé nhất để xóa seam
        
        int lowestEnergyCol = 0;
        // Bắt đầu từ hàng cuối cùng
        int r = height - 1;
        for (int c = 1; c < width; ++c) {
            if (energyTrack[r * originalWidth + c] < energyTrack[r * originalWidth + lowestEnergyCol]){
                lowestEnergyCol = c;
            }       
        }

        /*
        Đến đây, tìm được idx của hàng cuối cùng cần xóa:
        - Dịch pixel sang phải
         */
        while (r >= 0) {
            // Dịch pixel sang phải
            for (int i = lowestEnergyCol; i < width - 1; ++i) {
                outPixels[r * originalWidth + i] = outPixels[r * originalWidth + i + 1];
                grayPixels[r * originalWidth + i] = grayPixels[r * originalWidth + i + 1];
            }

            /*
            Cập nhật lại lowestEnergyCol cho lượt xóa sau:
            - Từ lowestEnergyCol ban đầu, lấy idx của hàng bên trên
            - Tìm giá trị energyTrack nhỏ nhất, set lại lowestEnergyCol
            
            */
             if (r > 0) {    
                int aboveIdx = (r - 1) * originalWidth + lowestEnergyCol;
                int min = energyTrack[aboveIdx];
                int _lowestEnergyCol = lowestEnergyCol;
                if (_lowestEnergyCol > 0 && energyTrack[aboveIdx - 1] < min) {
                    min = energyTrack[aboveIdx - 1];
                    lowestEnergyCol = _lowestEnergyCol - 1;
                }
                if (_lowestEnergyCol < width - 1 && energyTrack[aboveIdx + 1] < min) {
                    lowestEnergyCol = _lowestEnergyCol + 1;
                }
            }
            r--;
        }
        --width;
    }
    
    free(grayPixels);
    free(energyTrack);
    free(energy);

    timer.Stop();
    float time = timer.Elapsed();
    printf("Processing time (use device): %f ms\n\n", time);
}

void seamCarvingByHost(uchar3 *inPixels, int width, int height, int finalWidth, uchar3* outPixels) {
    GpuTimer timer;
    timer.Start();

    memcpy(outPixels, inPixels, width * height * sizeof(uchar3));

    // Tạo ảnh grayscale
    uint8_t *grayPixels= (uint8_t *)malloc(width * height * sizeof(uint8_t));
    convertRgb2Gray(inPixels, width, height,grayPixels);

    // Bảng energy và bảng
    int *energy = (int *)malloc(width * height * sizeof(int));
    int *energyTrack = (int *)malloc(width * height * sizeof(int));
    const int originalWidth = width;

    // Tính energy
    // for (int r = 0; r < height; ++r) {
    //     for (int c = 0; c < width; ++c) {
    //         energy[r * originalWidth + c] = computeEnergy(grayPixels,r,c,width,height);
    //     }
    // }

    // Thực hiện xóa seam đến khi nào đạt được width mong muốn
    while (width > finalWidth) {

        // Tính energy Table
        for (int r = 0; r < height; ++r) {
            for (int c = 0; c < width; ++c) {
                energy[r * originalWidth + c] = computeEnergy(grayPixels,r,c,width,height);
            }
        }

        // Với hàng đầu tiên: energyTrack = energy
        for (int c = 0; c < width; ++c) {
            energyTrack[c] = energy[c];
        }

        /*
        Các hàng sau đó, với mỗi cột:
        - Lấy index của hàng phía trên
        - Tìm giá trị min(energyTrack của hàng phía trên, hàng phía trên trái nếu có, hàng phía trên phải nếu có)
        - Lấy min + giá trị energy tại điểm đó ra giá trị eneryTrack mới 
        */

        for (int r = 1; r < height; ++r) {
            for (int c = 0; c < width; ++c) {
                int idx = r * originalWidth + c;
                int aboveIdx = (r - 1) * originalWidth + c;

                int min = energyTrack[aboveIdx];
                if (c > 0 && energyTrack[aboveIdx - 1] < min) {
                    min = energyTrack[aboveIdx - 1];
                }
                if (c < width - 1 && energyTrack[aboveIdx + 1] < min) {
                    min = energyTrack[aboveIdx + 1];
                }

                energyTrack[idx] = min + energy[idx];
            }
        }
        // Đến đây ta có bảng energyTrack, thực hiện tìm cột có eneryTrack bé nhất để xóa seam
        
        int lowestEnergyCol = 0;
        // Bắt đầu từ hàng cuối cùng
        int r = height - 1;
        for (int c = 1; c < width; ++c) {
            if (energyTrack[r * originalWidth + c] < energyTrack[r * originalWidth + lowestEnergyCol]){
                lowestEnergyCol = c;
            }       
        }

        /*
        Đến đây, tìm được idx của hàng cuối cùng cần xóa:
        - Dịch pixel sang phải
         */
        while (r >= 0) {
            // Dịch pixel sang phải
            for (int i = lowestEnergyCol; i < width - 1; ++i) {
                outPixels[r * originalWidth + i] = outPixels[r * originalWidth + i + 1];
                grayPixels[r * originalWidth + i] = grayPixels[r * originalWidth + i + 1];
                energy[r * originalWidth + i] = energy[r * originalWidth + i + 1];
            }

            /*
            Cập nhật lại lowestEnergyCol cho lượt xóa sau:
            - Từ lowestEnergyCol ban đầu, lấy idx của hàng bên trên
            - Tìm giá trị energyTrack nhỏ nhất, set lại lowestEnergyCol
            
            */
             if (r > 0) {    
                int aboveIdx = (r - 1) * originalWidth + lowestEnergyCol;
                int min = energyTrack[aboveIdx];
                int _lowestEnergyCol = lowestEnergyCol;
                if (_lowestEnergyCol > 0 && energyTrack[aboveIdx - 1] < min) {
                    min = energyTrack[aboveIdx - 1];
                    lowestEnergyCol = _lowestEnergyCol - 1;
                }
                if (_lowestEnergyCol < width - 1 && energyTrack[aboveIdx + 1] < min) {
                    lowestEnergyCol = _lowestEnergyCol + 1;
                }
            }
            r--;
        }
        --width;
    }
    
    free(grayPixels);
    free(energyTrack);
    free(energy);

    timer.Stop();
    float time = timer.Elapsed();
    printf("Processing time (use host): %f ms\n\n", time);
}

float computeError(uchar3 * a1, uchar3 * a2, int n)
{
    float err = 0;
    for (int i = 0; i < n; i++)
    {
        err += abs((int)a1[i].x - (int)a2[i].x);
        err += abs((int)a1[i].y - (int)a2[i].y);
        err += abs((int)a1[i].z - (int)a2[i].z);
    }
    err /= (n * 3);
    return err;
}

char *concatStr(const char * s1, const char * s2)
{
    char * result = (char *)malloc(strlen(s1) + strlen(s2) + 1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

void printDeviceInfo()
{
    cudaDeviceProp devProv;
    CHECK(cudaGetDeviceProperties(&devProv, 0));
    printf("**********GPU info**********\n");
    printf("Name: %s\n", devProv.name);
    printf("Compute capability: %d.%d\n", devProv.major, devProv.minor);
    printf("Num SMs: %d\n", devProv.multiProcessorCount);
    printf("Max num threads per SM: %d\n", devProv.maxThreadsPerMultiProcessor); 
    printf("Max num warps per SM: %d\n", devProv.maxThreadsPerMultiProcessor / devProv.warpSize);
    printf("GMEM: %lu bytes\n", devProv.totalGlobalMem);
    printf("CMEM: %lu bytes\n", devProv.totalConstMem);
    printf("L2 cache: %i bytes\n", devProv.l2CacheSize);
    printf("SMEM / one SM: %lu bytes\n", devProv.sharedMemPerMultiprocessor);

    printf("****************************\n\n");

}

// argv[1] = inputPnm, argv[2] = outPmn, argv[3] = finalWidth
int main(int argc, char ** argv)
{   
    if (argc != 4 && argc != 6)
    {
        printf("The number of arguments is invalid\n");
        return EXIT_FAILURE;
    }

    printDeviceInfo();

    // Read input RGB image file
    int width, height;
    uchar3 *inPixels;
    readPnm(argv[1], width, height, inPixels);
    printf("Image size (width x height): %i x %i\n\n", width, height);
    originalWidth = width;
    float finalWidth = stof(argv[3]);
    if (finalWidth <= 0 || finalWidth > width)
        return EXIT_FAILURE; // invalid finalWidth
    printf("FinalWidth Width : %f\n\n", finalWidth);

    // seam carving using host
    uchar3 * correctOutPixels = (uchar3 *)malloc(width * height * sizeof(uchar3));
    seamCarvingByHost(inPixels, width, height, finalWidth, correctOutPixels);

    uchar3 * outPixels= (uchar3 *)malloc(width * height * sizeof(uchar3));
    dim3 blockSize(32, 32); // Default
    if (argc == 6)
    {
        blockSize.x = atoi(argv[4]);
        blockSize.y = atoi(argv[5]);
    } 
    seamCarvingByDevice(inPixels, width, height, finalWidth, outPixels, blockSize);

    
    // Compute mean absolute error between host result and device result
    float err = computeError(outPixels, correctOutPixels, width * height);
    printf("Error between device result and host result: %f\n", err);

    // Write results to files
    char *outFileNameBase = strtok(argv[2], "."); // Get rid of extension
    writePnm(correctOutPixels, finalWidth, height, width, concatStr(outFileNameBase, "_host.pnm"));
    writePnm(outPixels, finalWidth, height, width, concatStr(outFileNameBase, "_device.pnm"));

    // Free memories
    free(inPixels);
    free(correctOutPixels);
}
